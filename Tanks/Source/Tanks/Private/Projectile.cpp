// Fill out your copyright notice in the Description page of Project Settings.

#include "Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "TankProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

AProjectile::AProjectile()
{
	ProjectileMovement = CreateDefaultSubobject<UTankProjectileMovementComponent>(FName("Projectile Movement"));
	ProjectileMovement->bAutoActivate = false;

	CollisionMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Collision Mesh"));
	SetRootComponent(CollisionMesh);
	CollisionMesh->SetNotifyRigidBodyCollision(true);
	CollisionMesh->SetVisibility(false);

	LaunchBlast = CreateDefaultSubobject<UParticleSystemComponent>(FName("Launch Blast"));
	LaunchBlast->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	ImpactBlast = CreateDefaultSubobject<UParticleSystemComponent>(FName("Impact Blast"));
	ImpactBlast->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	ImpactBlast->bAutoActivate = false;

	ExplosionForce = CreateDefaultSubobject<URadialForceComponent>(FName("ExplosionForce"));
	ExplosionForce->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

void AProjectile::LaunchProjectile(const float Speed) const
{
	ProjectileMovement->SetVelocityInLocalSpace(FVector::ForwardVector * Speed);
	ProjectileMovement->Activate();
}

void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	if (ensure(CollisionMesh))
	{
		CollisionMesh->OnComponentHit.AddDynamic(this, &AProjectile::OnHitEventAction);
	}
}

void AProjectile::OnHitEventAction(UPrimitiveComponent* HitComponent, AActor* OtherActor,
                                   UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	ImpactBlast->Activate();
	LaunchBlast->Deactivate();
	ExplosionForce->FireImpulse();
	UE_LOG(LogTemp, Warning, TEXT("HITTED %s "), *OtherActor->GetName())

	UGameplayStatics::ApplyRadialDamage(this, FMath::RandRange(Damage / 2, Damage), GetActorLocation(), ExplosionForce->Radius,
	                                    UDamageType::StaticClass(), TArray<AActor*>());
	SetRootComponent(ImpactBlast);
	CollisionMesh->DestroyComponent();
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &AProjectile::OnDestroyTimeHandle, DestroyDelay, false);
}

void AProjectile::OnDestroyTimeHandle()
{
	Destroy();
}
