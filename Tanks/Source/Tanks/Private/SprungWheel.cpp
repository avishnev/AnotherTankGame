// Fill out your copyright notice in the Description page of Project Settings.


#include "SprungWheel.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Components/SphereComponent.h"


// Sets default values
ASprungWheel::ASprungWheel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PostPhysics;

	WheelConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("WheelConstraint"));
	SetRootComponent(WheelConstraint);

	Axel = CreateDefaultSubobject<USphereComponent>(FName("Axel"));
	Axel->SetupAttachment(WheelConstraint);

	Wheel = CreateDefaultSubobject<USphereComponent>(FName("Wheel"));
	Wheel->SetupAttachment(Axel);

	AxelConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("AxelConstraint"));
	AxelConstraint->SetupAttachment(Axel);
}

void ASprungWheel::ApplyForce()
{
	Wheel->AddForce(Axel->GetForwardVector() * TotalForcePerFrame);
}

void ASprungWheel::AddDrivingForce(float ForceMagnitude)
{
	TotalForcePerFrame += ForceMagnitude;
}

void ASprungWheel::SetupPhysicsConstraint()
{
	if (GetAttachParentActor())
	{
		if (UPrimitiveComponent *BodyRoot = Cast<UPrimitiveComponent>(GetAttachParentActor()->GetRootComponent()))
		{
			WheelConstraint->SetConstrainedComponents(BodyRoot, NAME_None, Axel, NAME_None);
			AxelConstraint->SetConstrainedComponents(Axel, NAME_None, Wheel, NAME_None);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("SprungWheel is not attached to tanks track"))
	}
}

// Called when the game starts or when spawned
void ASprungWheel::BeginPlay()
{
	Super::BeginPlay();

	Wheel->OnComponentHit.AddDynamic(this, &ASprungWheel::OnHit);
	Wheel->SetNotifyRigidBodyCollision(true);
	SetupPhysicsConstraint();
}

void ASprungWheel::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	ApplyForce();
}

void ASprungWheel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetWorld()->TickGroup == TG_PostPhysics)
	{
		TotalForcePerFrame = 0;
	}
}

