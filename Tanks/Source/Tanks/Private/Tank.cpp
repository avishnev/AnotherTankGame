// Fill out your copyright notice in the Description page of Project Settings.

#include "Tank.h"

ATank::ATank()
{
	PrimaryActorTick.bCanEverTick = false;
}

float ATank::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	const int32 ApplyDamage = FMath::Clamp<int32>( FPlatformMath::RoundToInt(DamageAmount), 0, CurrentHealth);
	//TODO: Main tank  do not take damage find out why

	CurrentHealth -= ApplyDamage;

	UE_LOG(LogTemp, Warning, TEXT("%s recived %i"), *GetName(), ApplyDamage);
	
	if (CurrentHealth <= 0)
	{
		OnDeath.Broadcast();
	}
	return ApplyDamage;
}

float ATank::GetHealthPercentage() const 
{
	return static_cast<float>(CurrentHealth) / static_cast<float>(StartingHealth);
}

void ATank::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = StartingHealth;
}

