// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"

#include "Tank.h"
#include "TankAimingComponent.h"

ATankAIController::ATankAIController()
{
	bAllowTickBeforeBeginPlay = true;
}

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
}

void ATankAIController::OnTankOutOfHealth()
{
	UE_LOG(LogTemp, Warning, TEXT(" ATankAIControlle DIED"));
	GetPawn()->DetachFromControllerPendingDestroy();
}

void ATankAIController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if (InPawn)
	{
		ATank* PossessedPawn = Cast<ATank>(InPawn);
		if (!(ensure(PossessedPawn)))
		{
			return;
		}
		PossessedPawn->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnTankOutOfHealth);
	}
}

void ATankAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	APawn* PlayerTank = GetWorld()->GetFirstPlayerController()->GetPawn();
	APawn* ControlledTank = GetPawn();
	if (!(PlayerTank && ControlledTank)) { return; }
	UTankAimingComponent *AimingComponent = ControlledTank->FindComponentByClass<UTankAimingComponent>();

	AimingComponent->AimAt(PlayerTank->GetActorLocation());
	if (AimingComponent->GetFireState() == EFiringStatus::FS_ReadyToShoot)
	{
		AimingComponent->Fire();
	}
	MoveToActor(PlayerTank, AcceptanceRadius);
}
