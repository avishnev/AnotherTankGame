// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAimingComponent.h"
#include "Projectile.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#include "Kismet/GameplayStatics.h"

UTankAimingComponent::UTankAimingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();
	LastFireTime = FPlatformTime::Seconds();
}

void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                         FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (FPlatformTime::Seconds() - LastFireTime < ShootingCooldown)
	{
		FireState = EFiringStatus::FS_Reloading;
	}
	else if (IsBarrelMoving())
	{
		FireState = EFiringStatus::FS_Aiming;
	}
	else if (AmountOfAmmo <= 0)
	{
		FireState = EFiringStatus::FS_NoAmmoLeft;
	}
	else
	{
		FireState = EFiringStatus::FS_ReadyToShoot;
	}
}

bool UTankAimingComponent::IsBarrelMoving() const
{
	if (!ensure(Barrel)) { return false; }
	return !AimDirection.Equals(Barrel->GetForwardVector(), 0.01);
}

void UTankAimingComponent::AimAt(const FVector& HitLocation)
{
	if (!ensure(Barrel)) { return; }
	FVector OutLaunchVelocity;
	const FVector StartLocation = Barrel->GetSocketLocation(FName("Projectile"));

	if (UGameplayStatics::SuggestProjectileVelocity(
		this,
		OutLaunchVelocity,
		StartLocation,
		HitLocation,
		LaunchSpeed,
		false,
		0,
		0,
		ESuggestProjVelocityTraceOption::DoNotTrace,
		FCollisionResponseParams::DefaultResponseParam,
		TArray<AActor*>(),
		false
	))
	{
		AimDirection = OutLaunchVelocity.GetSafeNormal();
		MoveBarrelTowards();
	}
}

void UTankAimingComponent::Initialize(UTankBarrel* InBarrel, UTankTurret* InTurret)
{
	Barrel = InBarrel;
	Turret = InTurret;
}

void UTankAimingComponent::Fire()
{
	if (FireState != EFiringStatus::FS_Reloading && AmountOfAmmo > 0)
	{
		if (!ensure(Projectile_BP && Barrel)) { return; }
		AProjectile* ProjectTile = GetWorld()->SpawnActor<AProjectile>(
			Projectile_BP,
			Barrel->GetSocketLocation(FName("Projectile")),
			Barrel->GetSocketRotation(FName("Projectile"))
		);

		ProjectTile->LaunchProjectile(LaunchSpeed);
		AmountOfAmmo--;
		LastFireTime = FPlatformTime::Seconds();
		FireState = EFiringStatus::FS_Reloading;
	}
}

void UTankAimingComponent::MoveBarrelTowards() const
{
	const FRotator BarrelRotation = Barrel->GetForwardVector().Rotation();
	const FRotator AimAsRotator = AimDirection.Rotation();
	const FRotator DeltaRotation = AimAsRotator - BarrelRotation;

	Barrel->Elevate(DeltaRotation.Pitch);
	if (FMath::Abs(DeltaRotation.Yaw) > 180)
	{
		Turret->RotateTurret(-DeltaRotation.Yaw);
	}
	else
	{
		Turret->RotateTurret(DeltaRotation.Yaw);
	}
}
