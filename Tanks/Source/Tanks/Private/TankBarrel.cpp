// Fill out your copyright notice in the Description page of Project Settings.


#include "TankBarrel.h"

void UTankBarrel::Elevate(float RelativeSpeed)
{
	const float ElevationChange = FMath::Clamp<float>(RelativeSpeed, -1, 1) * MaxDegreesPerSeconds *
		GetWorld()->DeltaTimeSeconds;
	const float NewElevation = FMath::Clamp<float>(GetRelativeRotation().Pitch + ElevationChange,
	                                               MinimumElevation,
	                                               MaximumElevation);

	SetRelativeRotation(FRotator(NewElevation, 0, 0));
}
