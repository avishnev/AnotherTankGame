
#include "TankMovementComponent.h"
#include "TankTrack.h"

void UTankMovementComponent::Initialize(UTankTrack* Left, UTankTrack* Right)
{
	LeftTrack = Left;
	RigthTrack = Right;
}

void UTankMovementComponent::IntendMoveForward(const float Throw)
{
	if (ensure(LeftTrack && RigthTrack))
	{
		LeftTrack->SetThrottle(Throw);
		RigthTrack->SetThrottle(Throw);
	}
}

void UTankMovementComponent::IntendTurnRight(const float Throw)
{
	if (ensure(LeftTrack && RigthTrack))
	{
		LeftTrack->SetThrottle(-Throw);
		RigthTrack->SetThrottle(Throw);
	}
}

void UTankMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed)
{
	const FVector TankForward = GetOwner()->GetActorForwardVector().GetSafeNormal();
	const FVector AIForwardIntention = MoveVelocity.GetSafeNormal();

	IntendMoveForward(TankForward | AIForwardIntention);
	IntendTurnRight((TankForward ^ AIForwardIntention).Z); 
}
