// Fill out your copyright notice in the Description page of Project Settings.


#include "TankTrack.h"

#include "SpawnPoint.h"
#include "SprungWheel.h"

UTankTrack::UTankTrack()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UTankTrack::DriveTrack(float CurrentThrottle)
{
	float ForceApplied =  CurrentThrottle * TrackMaxDrivingForce;
	auto Wheels = GetWheels();
	float ForcePerWheel = ForceApplied / Wheels.Num();

	for (ASprungWheel* Wheel: Wheels)
	{
		Wheel->AddDrivingForce(ForcePerWheel);
	}
}

void UTankTrack::SetThrottle(const float Throttle)
{
	DriveTrack(FMath::Clamp<float>(Throttle, -1, 1));
}

void UTankTrack::BeginPlay()
{
	Super::BeginPlay();
	TankRootComponent = Cast<UStaticMeshComponent>(GetOwner()->GetRootComponent());
	ensure(TankRootComponent);
}

TArray<ASprungWheel*> UTankTrack::GetWheels() const
{
	TArray<ASprungWheel*> Result;
	TArray<USceneComponent*> Children;
	GetChildrenComponents(true, Children);

	for (auto Child: Children)
	{
		if (auto SpawnPoint = Cast<USpawnPoint>(Child))
		{
			if (auto Wheel = Cast<ASprungWheel>(SpawnPoint->GetSpawnedActor()))
			{
				Result.Add(Wheel);
			}
		}
	}
	return Result;
}
