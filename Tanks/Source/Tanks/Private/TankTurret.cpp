// Fill out your copyright notice in the Description page of Project Settings.


#include "TankTurret.h"

void UTankTurret::RotateTurret(const float RelativeSpeed)
{
	const float RotationChange = FMath::Clamp<float>(RelativeSpeed, -1, 1) * MaxDegreesPerSeconds *
		GetWorld()->DeltaTimeSeconds;
	const float NewRotation = GetRelativeRotation().Yaw + RotationChange;
	SetRelativeRotation(FRotator(0, NewRotation, 0));
}
