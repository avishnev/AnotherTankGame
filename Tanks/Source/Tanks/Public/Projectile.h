// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PhysicsEngine/RadialForceComponent.h"

#include "Projectile.generated.h"

class UTankProjectileMovementComponent;

UCLASS()
class TANKS_API AProjectile : public AActor
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	AProjectile();

	void LaunchProjectile(const float Speed) const;

protected:
	virtual void BeginPlay() override;

	UTankProjectileMovementComponent* ProjectileMovement = nullptr;

	UPROPERTY(VisibleAnywhere, Category="Components")
	UStaticMeshComponent* CollisionMesh = nullptr;

	UPROPERTY(VisibleAnywhere, Category="Components")
	UParticleSystemComponent* LaunchBlast = nullptr;

	UPROPERTY(VisibleAnywhere, Category="Components")
	UParticleSystemComponent* ImpactBlast = nullptr;

	UPROPERTY(VisibleAnywhere, Category="Components")
	URadialForceComponent* ExplosionForce = nullptr;

	UPROPERTY(BlueprintReadWrite, Category="Setup")
	float DestroyDelay = 10.f;

	UPROPERTY(BlueprintReadWrite, Category="Setup")
	float Damage = 70.f;

private:
	UFUNCTION()
	void OnHitEventAction(UPrimitiveComponent* HitComponent, AActor* OtherActor,
	                      UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	                      
	FTimerHandle TimerHandle;
	void OnDestroyTimeHandle();
};
