// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "SprungWheel.generated.h"

class USphereComponent;
class UPhysicsConstraintComponent;
UCLASS()
class TANKS_API ASprungWheel : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASprungWheel();
	void ApplyForce();

	void AddDrivingForce(float ForceMagnitude);
	void SetupPhysicsConstraint();
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere, Category="Components")
	USphereComponent* Wheel = nullptr;

	UPROPERTY(VisibleAnywhere, Category="Components")
	USphereComponent* Axel = nullptr;

	UPROPERTY(VisibleAnywhere, Category="Components")
	UPhysicsConstraintComponent* WheelConstraint = nullptr;

	UPROPERTY(VisibleAnywhere, Category="Components")
	UPhysicsConstraintComponent* AxelConstraint = nullptr;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	           FVector NormalImpulse, const FHitResult& Hit);
	float TotalForcePerFrame = 0;
};
