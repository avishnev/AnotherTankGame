// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "TankAIController.generated.h"

UCLASS()
class TANKS_API ATankAIController : public AAIController
{
	GENERATED_BODY()

public:
	ATankAIController();
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditDefaultsOnly, Category="Setup")
	float AcceptanceRadius = 8000.0f;

	UFUNCTION()
	void OnTankOutOfHealth();
private:
	virtual void SetPawn(APawn* InPawn) override;
	virtual void Tick(float DeltaSeconds) override;
};
