// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"

class UTankBarrel;
class UTankTurret;
class AProjectile;

UENUM()
enum class EFiringStatus : uint8
{
	FS_Reloading,
	FS_Aiming,
	FS_ReadyToShoot,
	FS_NoAmmoLeft
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TANKS_API UTankAimingComponent final : public UActorComponent
{
	GENERATED_BODY()

public:
	UTankAimingComponent();

	UTankBarrel* GetBarrel() const { return Barrel; }
	UTankTurret* GetTurret() const { return Turret; }
	void AimAt(const FVector& HitLocation);

	UFUNCTION(BlueprintCallable, Category = "Firing")
	void Fire();
	void MoveBarrelTowards() const;
	EFiringStatus GetFireState() const { return FireState; }


protected:
	UFUNCTION(BlueprintCallable, Category= "Setup")
	void Initialize(UTankBarrel* InBarrel, UTankTurret* InTurret);

protected:
	UPROPERTY(BlueprintReadOnly, Category= "State")
	EFiringStatus FireState = EFiringStatus::FS_Reloading;
	
	UPROPERTY(BlueprintReadOnly, Category= "State")
	int32 AmountOfAmmo = 5;

	UPROPERTY(EditDefaultsOnly, Category="Setup")
	TSubclassOf<AProjectile> Projectile_BP;

	UPROPERTY(EditDefaultsOnly, Category="Firing")
	float LaunchSpeed = 40000.0f; //1000 m/s

	UPROPERTY(EditDefaultsOnly, Category="Firing")
	float ShootingCooldown = 3;

private:
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;

	bool IsBarrelMoving() const;

	UTankBarrel* Barrel = nullptr;
	UTankTurret* Turret = nullptr;
	FVector AimDirection;
	double LastFireTime = 0;
};
