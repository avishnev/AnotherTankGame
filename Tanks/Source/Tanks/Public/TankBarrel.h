// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"
#include "TankBarrel.generated.h"

/**
* Used for Barrel elevation
*/
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TANKS_API UTankBarrel : public UStaticMeshComponent
{
	GENERATED_BODY()
public:
	// -1 is max downward speed, +1 is max up movement
	void Elevate(float RelativeSpeed);

private:
	UPROPERTY(EditDefaultsOnly, Category=Setup)
	float MaxDegreesPerSeconds = 10.0f;

	UPROPERTY(EditDefaultsOnly, Category=Setup)
	float MaximumElevation = 40.0f;

	UPROPERTY(EditDefaultsOnly, Category=Setup)
	float MinimumElevation = 0.0f;
};
