#pragma once

#include "CoreMinimal.h"

#include "GameFramework/NavMovementComponent.h"
#include "TankMovementComponent.generated.h"

class UTankTrack;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TANKS_API UTankMovementComponent : public UNavMovementComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category= "Setup")
	void Initialize(UTankTrack* Left, UTankTrack* Right);

	UFUNCTION(BlueprintCallable, Category= "Input")
	void IntendMoveForward(float Throw);

	UFUNCTION(BlueprintCallable, Category= "Input")
	void IntendTurnRight(float Throw);

private:
	virtual void RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed) override;

	UTankTrack* LeftTrack = nullptr;
	UTankTrack* RigthTrack = nullptr;
};
