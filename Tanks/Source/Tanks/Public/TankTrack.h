// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"

/**
 *  TankTrack is used for setting maximum driving force 
 */
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TANKS_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category=Input)
	void SetThrottle(float Throttle);

	virtual void BeginPlay() override;

private:
	explicit UTankTrack();

	void DriveTrack(float Throttle);

private:
	UPROPERTY()
	UStaticMeshComponent* TankRootComponent;
	// Max force in newtons
	UPROPERTY(EditDefaultsOnly, Category=Setup)
	float TrackMaxDrivingForce = 350000.0f;
	TArray<class ASprungWheel*> GetWheels() const;
};
